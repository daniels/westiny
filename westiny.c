/*
 * Copyright © 2018 Collabora, Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Author: Daniel Stone <daniels@collabora.com>
 */

#include <assert.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <wayland-server.h>
#include <compositor.h>
#include <compositor-wayland.h>
#include <libweston-desktop.h>
#include <windowed-output-api.h>

/* Core server structures */
struct westiny_compositor {
	struct {
		/* The core libweston compositor */
		struct weston_compositor *compositor;

		struct weston_head *head;
	} libweston;

	struct {
		/* Wayland display that clients connect to */
		struct wl_display *display;
		/* File descriptor to listen on for new activity */
		int fd;
	} wl_server;
};

struct westiny_output {
	struct westiny_compositor *compositor;

	const struct weston_windowed_output_api *api;
	struct weston_head *head;
	struct weston_output *output;

	struct wl_listener heads_changed_cb;
};

struct westiny_backend {
	struct westiny_compositor *compositor;
	struct weston_wayland_backend_config config;
};

struct westiny_desktop {
	struct westiny_compositor *compositor;
	struct {
		struct weston_desktop *desktop;
		struct weston_layer layer;
	} libweston;
};

struct westiny_desktop_surface {
	struct westiny_desktop *desktop;
	struct {
		struct weston_desktop_surface *desktop_surface;
		struct weston_view *view;
	};
};

/*
 * XXX: creating outputs
 *      weston_compositor_set_heads_changed_cb();
 *      weston_windowed_output_get_api();
 *      api->create_head();
 *        -> heads_changed_cb();
 *           -> weston_compositor_create_output_with_head();
 *           -> api->output_set_size();
 *           -> weston_output_set_scale();
 *           -> weston_output_set_transform();
 *           -> weston_output_enable();
 */
static void heads_changed_cb(struct wl_listener *listener, void *data)
{
	struct westiny_output *wo = wl_container_of(listener, wo, heads_changed_cb);
	struct westiny_compositor *wt = wo->compositor;
	struct weston_head *head = NULL;

	while ((head = weston_compositor_iterate_heads(wt->libweston.compositor, head))) {
		if (head->output)
			continue;

		wo->output =
			weston_compositor_create_output_with_head(wt->libweston.compositor,
								  head);
		assert(wo->output);
		weston_output_set_scale(wo->output, 1);
		weston_output_set_transform(wo->output, WL_OUTPUT_TRANSFORM_NORMAL);
		wo->api->output_set_size(wo->output, 1024, 768);
		weston_output_enable(wo->output);
	}
}

void add_output(struct westiny_compositor *wt)
{
	struct westiny_output *wo = calloc(1, sizeof(*wo));

	wo->compositor = wt;
	wo->api = weston_windowed_output_get_api(wt->libweston.compositor);
	assert(wo->api);

	wo->heads_changed_cb.notify = heads_changed_cb;
	weston_compositor_add_heads_changed_listener(wt->libweston.compositor,
						     &wo->heads_changed_cb);

	wo->api->create_head(wt->libweston.compositor, "westiny");
	//weston_compositor_flush_heads_changed(wt->libweston.compositor);
}

void load_backend(struct westiny_compositor *wt)
{
	struct westiny_backend *wb = calloc(1, sizeof(*wb));
	int ret;

	wb->compositor = wt;
	wb->config = (struct weston_wayland_backend_config) {
		.base = {
			.struct_size = sizeof(wb->config),
			.struct_version = 2,
		},
		.sprawl = false,
		.fullscreen = false,
		.use_pixman = false,
		.cursor_theme = NULL,
		.cursor_size = 32,
	};

	ret = weston_compositor_load_backend(wt->libweston.compositor,
					     WESTON_BACKEND_WAYLAND,
					     &wb->config.base);
	assert(ret == 0);
}

static void configure_input(struct westiny_compositor *wt)
{
	/* If you don't do this, Weston will crash trying to compile a
	 * keymap. */
	weston_compositor_set_xkb_rule_names(wt->libweston.compositor, NULL);
}

static void desktop_surface_added(struct weston_desktop_surface *desktop_surface,
				  void *data)
{
	struct westiny_desktop_surface *ws = calloc(1, sizeof(*ws));
	struct westiny_desktop *wd = data;

	ws->desktop = data;
	ws->desktop_surface = desktop_surface;

	ws->view = weston_desktop_surface_create_view(ws->desktop_surface);
	assert(ws->view);

	weston_layer_entry_insert(&wd->libweston.layer.view_list, &ws->view->layer_link);
        weston_desktop_surface_propagate_layer(ws->desktop_surface);
        weston_view_geometry_dirty(ws->view);
        //weston_surface_damage(ws->surface);
}

static void desktop_surface_removed(struct weston_desktop_surface *desktop_surface,
				    void *data)
{
	struct westiny_desktop_surface *ws =
		weston_desktop_surface_get_user_data(desktop_surface);

	weston_view_destroy(ws->view);
	free(ws);
}

static const struct weston_desktop_api westiny_desktop_handlers = {
	.struct_size = sizeof(westiny_desktop_handlers),
	.ping_timeout = NULL,
	.pong = NULL,
	.surface_added = desktop_surface_added, /* mandatory */
	.surface_removed = desktop_surface_removed, /* mandatory */
	.committed = NULL,
	.show_window_menu = NULL,
	.set_parent = NULL,
	.move = NULL,
	.resize = NULL,
	.fullscreen_requested = NULL,
	.maximized_requested = NULL,
	.minimized_requested = NULL,
};

static void init_desktop_shell(struct westiny_compositor *wt)
{
	struct westiny_desktop *wd = calloc(1, sizeof(*wd));

	wd->compositor = wt;
	wd->libweston.desktop =
		weston_desktop_create(wt->libweston.compositor,
				      &westiny_desktop_handlers, wd);
	assert(wd->libweston.desktop);

	weston_layer_init(&wd->libweston.layer, wt->libweston.compositor);
	weston_layer_set_position(&wd->libweston.layer,
				  WESTON_LAYER_POSITION_NORMAL);
}

void __attribute__((noreturn)) do_exit(struct weston_compositor *compositor)
{
	struct westiny_compositor *wt =
		weston_compositor_get_user_data(compositor);
	int exit_code = compositor->exit_code;

	/* Shut down the compositor and the socket. */
	weston_compositor_shutdown(wt->libweston.compositor);
	wl_display_terminate(wt->wl_server.display);
	free(wt); /* leaks outputs etc */

	exit(exit_code);
}

int main(int argc, char *argv[])
{
	struct westiny_compositor *wt = calloc(1, sizeof(*wt));

	/* Create a Wayland server, and add a socket to listen to
	 * clients on; give it a non-default name so no-one will connect
	 * to it by accident. */
	wt->wl_server.display = wl_display_create();
	assert(wt->wl_server.display);
	wl_display_add_socket(wt->wl_server.display, "westiny");

	/* Create the core libweston compositor structure, listening for new
	 * clients on the listening socket we've given it. */
	wt->libweston.compositor =
		weston_compositor_create(wt->wl_server.display, wt);
	assert(wt->libweston.compositor);
	wt->libweston.compositor->exit = do_exit;

	/* We are required to set a handler for log messages. */
	weston_log_set_handler(vprintf, vprintf);

	configure_input(wt);

	load_backend(wt);

	add_output(wt);

	init_desktop_shell(wt);

	/* Run the Wayland server event loop, until it gets told to shut down.
	 * libweston adds its hooks to the Wayland server event loop, so we
	 * don't need to do anything special here. */
	wl_display_run(wt->wl_server.display);

	do_exit(wt->libweston.compositor);
}